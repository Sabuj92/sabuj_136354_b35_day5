  <html>
	<head>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">


	</head>
	<body>


		
		<div class="container">
			<h2>Upload Files</h2>
			<form class="form-horizontal" role="form" action="file_post.php" method="post" enctype="multipart/form-data">
				<div class="form-group">
					<label class="control-label col-sm-2" for="text">Name</label>
					<div class="col-sm-5">
						<input type="text" class="form-control" id="text" placeholder="Enter Name" name="name">
					</div>
				</div>
				<div class="form-group">
					<label class="control-label col-sm-2" for="upload">Upload File</label>
					<div class="col-sm-5">
						<input type="file" class="form-control" id="file" placeholder="Choose File" name="FileToUpload">
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<button type="submit" class="btn btn-default" value="submit">Submit</button>
					</div>
				</div>
			</form>
		</div>

	
	
	
	
	
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		</body>

</html>